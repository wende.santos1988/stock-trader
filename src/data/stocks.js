export default [
    {id: 1, name: 'Facebook', price: 110},
    {id: 2, name: 'Amazon', price: 220},
    {id: 3, name: 'Tesla', price: 330},
    {id: 4, name: 'SpaceX', price: 440},
]